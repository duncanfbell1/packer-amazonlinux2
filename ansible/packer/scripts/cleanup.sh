#!/usr/bin/env bash

export HISTSIZE=0
sudo yum -y install yum-utils
sudo package-cleanup -y --oldkernels --count=1
sudo package-cleanup --quiet --leaves --exclude-bin | sudo xargs yum remove -y
sudo yum -y autoremove
sudo yum -y remove yum-utils
sudo yum clean all
sudo rm -rvf /tmp/*
sudo rm -rvf /var/tmp/yum-*
sudo rm -vf /var/log/wtmp /var/log/btmp
sudo rm -rvf /var/cache/yum

# Zero out the rest of the free space using dd, then delete the written file.
echo ":INFO:  Zeroing disk"
sudo dd if=/dev/zero of=/EMPTY bs=1M
sudo rm -vf /EMPTY

# Add `sync` so Packer doesn't quit too early, before the large file is deleted.
sync

exit 0
