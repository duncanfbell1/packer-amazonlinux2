#!/usr/bin/env bash

UUID=$(vboxmanage list hdds | grep inaccessible -B 2 | grep UUID | head -n 1 | awk '{print $2}')

if [ "${UUID}" != "" ] ; then
    vboxmanage closemedium disk "${UUID}" --delete
fi
