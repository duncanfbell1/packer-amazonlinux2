#!/usr/bin/env bash

DOWNLOAD="${1}"

function check_version {
    URL="$(curl -sIL https://cdn.amazonlinux.com/os-images/latest/virtualbox/ | grep -i location | cut -d' ' -f2)"
    URL="${URL/$'\r'/}"
    version="$(echo ${URL} | cut -d'/' -f5)"
    file_name="amzn2-virtualbox-${version}-x86_64.xfs.gpt.vdi"
    printf '{"VDI":"%s","VERSION":"%s"}\n' "${file_name}" "${version}"
}

function download_file {
    [ ! -d /tmp/packer ] && mkdir -pv /tmp/packer || :
    
    if [ -f /tmp/packer/"${file_name}" ]; then
        echo 'File already exists' >&2
        exit 0
    fi
    curl -o /tmp/packer/"${file_name}" "${URL}${file_name}"
}

if [ "${DOWNLOAD}" == "" ] ; then
    echo "Pass check or download as params"
    exit 1
elif [ "${DOWNLOAD}" == "check" ] ; then
    check_version
elif [ "${DOWNLOAD}" == "download" ] ; then
    check_version
    download_file
fi


