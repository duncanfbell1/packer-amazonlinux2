#!/usr/bin/env bash

tail -f /var/log/cloud-init.log | while read LOGLINE ; do
    echo ":CLOUDINIT:  ${LOGLINE}"
    [ -f "/var/lib/cloud/instance/boot-finished" ] && pkill -P $$ tail
done

# 4.19 kernel? ;-/
# sudo amazon-linux-extras install kernel-ng
sudo yum upgrade -y
sudo yum install -y "kernel-devel-uname-r == $(uname -r)"
