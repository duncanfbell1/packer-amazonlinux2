#!/usr/bin/env bash
set -euxo pipefail

VBoxURL="https://download.virtualbox.org/virtualbox/6.1.12/VBoxGuestAdditions_6.1.12.iso"
VBoxSHA="226eef0bb337a8375f6b659168c6eaf98b74a68782b9885b40ce9443fdb2ac16"

function check_sha() {
  echo "INFO:  Checking Guest Additions SHA"
  iso_sha="$(sha256sum VBoxGuestAdditions.iso | cut -d' ' -f1)"
  if [[ "${iso_sha}" != "${VBoxSHA}" ]] ; then
    download_iso
  fi
}

function download_iso() {
  echo "INFO:  Downloading Guest Additions"
  [ -f VBoxGuestAdditions.iso ] && rm -rfv VBoxGuestAdditions.iso || :
  curl -o VBoxGuestAdditions.iso ${VBoxURL}
}

echo "INFO:  Attempting Guest Additions Install"
if [ -f VBoxGuestAdditions.iso ] ; then
    check_sha
    echo "INFO:  Installing Guest Additions"
    DIR=$(mktemp -d)
    sudo mount -t iso9660 -o loop VBoxGuestAdditions.iso "${DIR}"
    ${DIR}/VBoxLinuxAdditions.run
    sudo umount "${DIR}"
    sudo rm -rfv "${DIR}" "VBoxGuestAdditions.iso"
else
    echo "ERROR:  Guest Additions ISO Missing"
    download_iso
    find . -name VBoxGuestAdditions.iso
fi

if which lsmod ; then
  if ! lsmod | grep vboxguest; then
    echo ":ERROR: VBoxGuestAdditions Not Installed Correctly"
    exit 1
  fi
fi
