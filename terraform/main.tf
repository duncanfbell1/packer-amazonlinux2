resource packet_ssh_key host_key {
  name       = "host_key"
  public_key = file(var.public_key_filename)
}

resource packet_device buildservers {
  project_id       = var.packet_project_id
  facilities       = var.facilities
  plan             = var.buildserver_plan
  operating_system = var.operating_system
  hostname         = format("%s-%02d", var.hostname, count.index)
  count            = var.buildserver_count
  billing_cycle    = "hourly"
  tags             = ["buildserver"]

  connection {
    user        = "root"
    private_key = file(var.private_key_filename)
  }

  depends_on = [packet_ssh_key.host_key]
}
