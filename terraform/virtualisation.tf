resource null_resource virtualisation {

  count = var.buildserver_count

  connection {
    type        = "ssh"
    host        = element(packet_device.buildservers.*.access_public_ipv4, count.index)
    private_key = file(var.private_key_filename)
    agent       = false
    timeout     = "10m"
  }

  provisioner "file" {
    source      = "scripts/virtual-setup.sh"
    destination = "/tmp/virtual-setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/virtual-setup.sh",
      "/tmp/virtual-setup.sh",
    ]
  }
  depends_on = [packet_device.buildservers]
}
