#!/bin/bash

echo "Running $(basename ${0})"

PACKER_VERSION=1.6.1
VIRTUALBOX_VERSION=6.1
VAGRANT_VERSION=2.2.9
# https://releases.hashicorp.com/packer/1.5.3/packer_1.5.3_linux_amd64.zip
PACKER_URL=https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip

sudo export DEBIAN_FRONTEND=noninteractive
sudo locale-gen en_GB.UTF-8

# Install Virtualbox 6.1
echo "deb http://download.virtualbox.org/virtualbox/debian bionic contrib" >> /etc/apt/sources.list
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo apt-get update

sudo DEBIAN_FRONTEND=noninteractive apt install -y git unzip curl dkms build-essential \
                    linux-headers-$(uname -r) x11-common x11-xserver-utils libxtst6 libxinerama1 psmisc

sudo DEBIAN_FRONTEND=noninteractive apt install -y -qq virtualbox-${VIRTUALBOX_VERSION}

# Fix broken things... hopefully
if ! command -v VBoxManage > /dev/null 2>&1; then
    echo "Manually download VirtualBox 6.1"
    wget https://download.virtualbox.org/virtualbox/6.1.12/virtualbox-6.1_6.1.12-139181~Ubuntu~bionic_amd64.deb
    sudo dpkg -i virtualbox-6.1_6.1.12-139181~Ubuntu~bionic_amd64.deb
    sudo DEBIAN_FRONTEND=noninteractive apt --fix-broken install -y
fi

# Install VirtualBox extension pack
vbox=$(VBoxManage --version)
vboxversion=${vbox%r*}
vboxrevision=${vbox#*r}
wget https://download.virtualbox.org/virtualbox/${vboxversion}/Oracle_VM_VirtualBox_Extension_Pack-${vboxversion}-${vboxrevision}.vbox-extpack
yes | VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-${vboxversion}-${vboxrevision}.vbox-extpack
rm -v Oracle_VM_VirtualBox_Extension_Pack-${vboxversion}-${vboxrevision}.vbox-extpack

# Install KVM
sudo DEBIAN_FRONTEND=noninteractive apt install -y qemu qemu-kvm libvirt-bin bridge-utils virt-manager

# Install Packer
mkdir /opt/packer
pushd /opt/packer
echo "Downloading packer ${PACKER_VERSION} ...."
wget ${PACKER_URL}
echo "Installing packer ${PACKER_VERSION} ..."
unzip packer_${PACKER_VERSION}_linux_amd64.zip
rm packer_${PACKER_VERSION}_linux_amd64.zip
pushd /usr/bin
ln -s /opt/packer/* .
popd
popd

# Install Vagrant
wget https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/vagrant_${VAGRANT_VERSION}_x86_64.deb
sudo dpkg -i vagrant_${VAGRANT_VERSION}_x86_64.deb
vagrant --version

echo "Installation Successful"
exit 0
