#!/bin/sh
TOKEN="${1}"
[ "${TOKEN}" = "" ] && echo "Pass API Token" && exit 1 || :
curl -s -X GET --header "Accept: application/json" --header "X-Auth-Token: ${TOKEN}" "https://api.packet.net/ssh-keys" | jq
