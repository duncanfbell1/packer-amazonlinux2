provider packet {
  version    = "~> 3.0"
  auth_token = var.auth_token
}

provider null {
  version = "~> 2.0"
}

terraform {
  required_providers {
    packet = {
      source  = "terraform-providers/packet"
      version = "~> 3.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "~> 2.1"
    }
  }
}
